
Punjab Industrial Establishments (National and Festival Holidays and Casual and Sick Leave) Act, 1965
=====================================================================================================

No: **14** Dated: **Jun, 22 1965**

**The Punjab Industrial Establishments (National and Festival Holidays and Casual and Sick Leave) Act, 1965**

**Punjab Act 14 of 1965**

    An Act to provide for the grant of National and Festival Holidays and Casual and Sick Leave to persons employed in Industrial Establishments in the State of Punjab.

    Be it enacted by the Legislature of the State of Punjab in the Sixteenth Year of the Republic of India as Follows: -

**1\. Short title, extent and commencement. - (1)** This Act may be called the **Punjab Industrial Establishments (National and Festival Holidays and Casual and Sick Leave) Act, 1965.**

**(2)** It extends to the whole of the State of Punjab.

**(3)** It shall come into force on the 1st day of July, 1965.

**2\. Definitions.** - In this Act, unless the context otherwise requires -

**(a) "day"** means a period of twenty-four hours beginning at mid-night:

Provided that in the case of a worker who works in a shift which extends beyond mid-night, such period of twenty-four hours shall begin when his shift ends;

**(b) "worker"** means -

**(i)** any person (including an apprentice) employed in any industrial establishment to do any skilled or unskilled, manual, supervisory, technical or clerical work for hire or reward, whether the terms of employment be expressed or implied; or

**(ii)** any other person employed in any industrial establishment whom the Government may, by notification, declare to be a worker for the purposes of this Act;

**(c) "employer"**, when used in relation to an industrial establishment, means the person who has ultimate control over the affairs of the industrial establishment, and, where the affairs of any industrial establishment are entrusted to any other person, whether called a managing agent, manager, superintendent, or by any other name, such other person shall be deemed to be the employer;

**(d) "Government"** means the Government of the State of Punjab;

**(e) "Industrial establishment"** means -

**(i)** any factory as defined in clause (m) of section 2 of the Factories Act, 1948 (Central Act 63 of 1948), or any place which is deemed to be a factory under sub-section (2) of section 85 of hat Act; or

**(ii)** any plantation as defined in clause (f) of section 2 of the Plantation Labour Act, 1951 (Central Act 69 of 1951);

**(f) "Inspector"** means an Inspector appointed under sub-section (1) of section 7;

**(g) "prescribed"** means prescribed by rules made under this Act;

**(h) "wages"** means all remuneration (whether by way of salary, allowances or otherwise) expressed in terms of money or capable of being so expressed which would, if the terms of employment, express or implied, were fulfilled, be payable to a worker in respect of his employment or of work done in such employment, but does not include -

**(a)** any bonus;

**(b)** the value of any house accommodation, supply of light, water, medical facilities or other amenity or of any service or of any concessional supply of foodgrains or other articles;

**(c)** any contribution paid or payable by the employer :-

**(i)** to any pension or provident fund, and the interest which may have accrued thereon; or

**(ii)** for the benefit of the worker under any law for the time being in force;

**(d)** any travelling allowance or the value of any travelling concession;

**(e)** any sum paid to the worker to defray special expenses entailed on him by the nature of his employment; or

**(f)** any gratuity payable on the termination of employment.

**3\. National and Festival holidays.** - (1) Every worker shall, in each calendar year, be allowed in such manner and on such conditions as may be prescribed -

**(a)** three national holidays of one whole day each on the 26th January, 15th August and 2nd October; and

**(b)** four other holidays on any of the festivals specified in the Schedule appended to this Act :

Provided that for purposes of clause (b), where at least ten per centum of the workers of an industrial establishment so desire, they may, in lieu of the festival holidays, avail of two half holidays on any of the festival days of their choice specified in such Schedule after settlement in this behalf has been made between the employer and the representative of the workers in such manner as may be prescribed,

    **(2)** The Government may, by notification, add to the Schedule appended to this Act any festival and thereupon the Schedule shall be deemed to be amended accordingly.
